from dataclasses import dataclass
from datetime import datetime
from stock_exchange.order import Order, OrderType
from stock_exchange.oms import OMS
from stock_exchange.exceptions import StockExchangeClosedException
from stock_exchange.stock import Stock


@dataclass
class StockExchange:
    stocks: set[Stock]
    is_open: bool
    oms: OMS

    @classmethod
    def open(cls, stocks: set[Stock]):
        return cls(stocks, is_open=True)

    def close(self):
        self.is_open = False

    def buy(self, stock: Stock, quantity: int, price: float):
        if not self.is_open:
            raise StockExchangeClosedException
        if quantity > 0:
            order = Order(
                stock=stock,
                created=datetime.datetime.now(),
                type=OrderType.BUY,
                quantity=quantity,
                price=price,
            )
            OMS.place_order(order)

    def sell(self, stock: Stock, quantity: int, price: float):
        if not self.is_open:
            raise StockExchangeClosedException
        if quantity > 0:
            order = Order(
                stock=stock,
                created=datetime.datetime.now(),
                type=OrderType.SELL,
                quantity=quantity,
                price=price,
            )
            self.oms.place_order(order)

    def find(self, slug: str):
        return [stock for stock in self.stocks if slug in (stock.name, stock.symbol)]
