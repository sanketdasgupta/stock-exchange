from stock_exchange.order import Order, OrderStatus, OrderType


class OMS:
    orders: list[Order]

    def __post_init__(self, order: Order):
        self.orders.append(order)

    def place_order(self, new_order: Order):
        # Basic stock matching algorithm
        pending_inverse_orders = sorted(
            (
                order
                for order in self.orders
                if order.status == OrderType.QUEUED
                and order.quantity == new_order.quantity
                and order.type == OrderType.get_inverse(order.type)
                and order.price == new_order.price
            ),
            key=lambda order: order.created,
            reverse=True,
        )
        matched_order = pending_inverse_orders[0]
        self.place_matched_order(new_order, matched_order)

    def place_matched_order(new_order=Order, matched_order=Order):
        new_order.status = matched_order.status = OrderStatus.COMPLETED

    def get_pending_orders(self):
        pass
